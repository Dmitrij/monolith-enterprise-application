/*
 * |-------------------------------------------------
 * | Copyright © 2018 Colin But. All rights reserved.
 * |-------------------------------------------------
 */
package com.mycompany.entapp.snowman.client.messaging;


import com.mycompany.entapp.snowman.client.messaging.dto.ClientDTO;

public interface InvoiceSystemPort {
    void sendProjectInfo(ClientDTO clientDTO);
}
