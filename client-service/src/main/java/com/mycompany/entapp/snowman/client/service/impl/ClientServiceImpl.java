/*
 * |-------------------------------------------------
 * | Copyright © 2018 Colin But. All rights reserved.
 * |-------------------------------------------------
 */
package com.mycompany.entapp.snowman.client.service.impl;

import com.mycompany.entapp.snowman.client.db.dto.Client;
import com.mycompany.entapp.snowman.client.db.repository.ClientRepository;
import com.mycompany.entapp.snowman.client.service.ClientService;
import com.mycompany.entapp.snowman.client.service.exception.SnowmanException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {

    private static final Logger LOG = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Client getClient(int clientId) {
        Client client = clientRepository.getClient(clientId);

        LOG.info("Retrieved client: {}", client);

        // TODO if we need project here than we can use Project resource to retrieve them
        //  or consider to have them part of clients
       /* if (client.getProjects().isEmpty()) {
            // call Client System REST endpoint to get its project data.

            ResponseEntity<String> response = makeRequest();

            // retry
            int retryCount = 0;
            while(response.getStatusCode() != HttpStatus.OK) {
                if (retryCount > MAX_RETRIES) {
                    break;
                }

                response = makeRequest();
                retryCount++;
            }

            processResponse(response.getBody(), client);
        }*/

        return client;
    }

    @Override
    public void createClient(Client client) throws SnowmanException {

        LOG.info("Creating client {}", client);

        if (getClient(client.getId()) != null) {
            throw new SnowmanException("Client already exists");
        }

        clientRepository.createClient(client);
    }

    @Override
    public void updateClient(Client client) throws SnowmanException {

        LOG.info("Updating client {}", client);

        if (getClient(client.getId()) == null) {
            throw new SnowmanException("Client doesn't exists");
        }

        clientRepository.updateClient(client);
    }

    @Override
    public void deleteClient(int clientId) throws SnowmanException {

        LOG.info("Deleting client with id {}", clientId);

        if (getClient(clientId) == null) {
            LOG.error("Trying to delete a client with id {} that doesn't exist", clientId);
            throw new SnowmanException("Client doesn't exists");
        }

        clientRepository.deleteClient(clientId);
    }
}
