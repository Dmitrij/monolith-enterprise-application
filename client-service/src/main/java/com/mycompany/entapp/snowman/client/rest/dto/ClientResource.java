/*
 * |-------------------------------------------------
 * | Copyright © 2018 Colin But. All rights reserved.
 * |-------------------------------------------------
 */
package com.mycompany.entapp.snowman.client.rest.dto;

public class ClientResource {

    private int clientId;
    private String clientName;
    // TODO that field belong to another business model
    // if need combination in one response than it should be moved on higher lvl µservice,
    // some kind of gateway
    //private List<ProjectResource> projects;

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

}
