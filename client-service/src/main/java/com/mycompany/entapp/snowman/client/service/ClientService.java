/*
 * |-------------------------------------------------
 * | Copyright © 2018 Colin But. All rights reserved.
 * |-------------------------------------------------
 */
package com.mycompany.entapp.snowman.client.service;


import com.mycompany.entapp.snowman.client.db.dto.Client;
import com.mycompany.entapp.snowman.client.service.exception.SnowmanException;

public interface ClientService {

    Client getClient(int clientId);

    void createClient(Client client) throws SnowmanException;

    void updateClient(Client client) throws SnowmanException;

    void deleteClient(int clientId) throws SnowmanException;

}
