package com.mycompany.entapp.snowman.client.messaging.dto;

import java.io.Serializable;

public class ClientDTO implements Serializable {

    private Integer clientId;

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }
}
