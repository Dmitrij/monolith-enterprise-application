/*
 * |-------------------------------------------------
 * | Copyright © 2018 Colin But. All rights reserved.
 * |-------------------------------------------------
 */
package com.mycompany.entapp.snowman.client.service.exception;

public class SnowmanException extends BusinessException {

    public SnowmanException(String message) {
        super(message);
    }
}
