/*
 * |-------------------------------------------------
 * | Copyright © 2018 Colin But. All rights reserved.
 * |-------------------------------------------------
 */
package com.mycompany.entapp.snowman.client.service.exception;

public class BusinessException extends Exception {

    public BusinessException(String message){
        super(message);
    }
}
