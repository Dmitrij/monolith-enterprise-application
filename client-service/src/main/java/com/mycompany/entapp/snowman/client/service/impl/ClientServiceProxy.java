package com.mycompany.entapp.snowman.client.service.impl;

import com.mycompany.entapp.snowman.client.db.dto.Client;
import com.mycompany.entapp.snowman.client.service.ClientService;
import com.mycompany.entapp.snowman.client.service.exception.SnowmanException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceProxy implements ClientService {

    @Autowired
    private ClientService clientServiceImpl;

    @Override
    public Client getClient(int clientId) {
        return clientServiceImpl.getClient(clientId);
    }

    @Override
    public void createClient(Client client) throws SnowmanException {
        clientServiceImpl.createClient(client);
    }

    @Override
    public void updateClient(Client client) throws SnowmanException {
        clientServiceImpl.updateClient(client);
    }

    @Override
    public void deleteClient(int clientId) throws SnowmanException {
        clientServiceImpl.deleteClient(clientId);
    }
}
