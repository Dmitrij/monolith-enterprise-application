/*
 * |-------------------------------------------------
 * | Copyright © 2018 Colin But. All rights reserved.
 * |-------------------------------------------------
 */
package com.mycompany.entapp.snowman.client.rest.mapper;

import static org.junit.Assert.assertEquals;

import com.mycompany.entapp.snowman.client.db.dto.Client;
import com.mycompany.entapp.snowman.client.rest.dto.ClientResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class ClientResourceMapperUTest {

    @Test
    public void testMapToClient() throws Exception {
        int clientId = 1;
        String clientName = "Client";

        ClientResource clientResource = new ClientResource();
        clientResource.setClientId(clientId);
        clientResource.setClientName(clientName);

        Client client = ClientResourceMapper.mapToClient(clientResource);

        assertEquals(clientId, client.getId());
        assertEquals(clientName, client.getClientName());
    }

    @Test
    public void testMapToClientResource() throws Exception {
        int clientId = 1;
        String clientName = "Client";

        Client client = new Client();
        client.setId(clientId);
        client.setClientName(clientName);

        // TODO move to project service
        /*List<ProjectResource> projectResources = new ArrayList<>();
        ProjectResource projectResource = new ProjectResource();
        projectResource.setProjectId(1);
        projectResource.setTitle("Project");
        projectResources.add(projectResource);

        PowerMockito.mockStatic(ProjectResourceMapper.class);

        PowerMockito.when(ProjectResourceMapper.mapToProjectResources(Matchers.anySetOf(Project.class)))
            .thenReturn(projectResources);
*/
        ClientResource clientResource = ClientResourceMapper.mapToClientResource(client);

        assertEquals(clientId, clientResource.getClientId());
        assertEquals(clientName, clientResource.getClientName());
        //assertEquals(projectResources, clientResource.getProjects());
    }

}
